## Available Scripts

In the project directory, you can run:

### `npm run start`

Run node server locally. Make sure you have created a production build before.

### `npm run start-dev`

Run node server locally in developement mode.

For more details check package.json file
