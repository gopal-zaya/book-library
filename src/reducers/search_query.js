const search_query = (state = "", action) => {
  switch (action.type) {
    case "SEARCH":
      return action.query;
    default:
      return state;
  }
};

export default search_query;
