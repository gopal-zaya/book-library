import { combineReducers } from "redux";
import books from "./books";
import search_query from "./search_query";
import book_navigation from "./book_navigation";

const BookLibrary = combineReducers({
  books,
  search_query,
  book_navigation
});

export default BookLibrary;
