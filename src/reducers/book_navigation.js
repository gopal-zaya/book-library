import { CATEGORIES } from "../actions";
const book_navigation = (
  state = {
    index: 0,
    category: CATEGORIES.CURRENT.val
  },
  action
) => {
  switch (action.type) {
    case "SWITCH_MENU":
      return {
        index: action.index,
        category: action.category
      };
    default:
      return state;
  }
};

export default book_navigation;
