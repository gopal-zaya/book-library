const BOOK_LIST = require("../data/data.json");

const books = (state = BOOK_LIST, action) => {
  switch (action.type) {
    case "WISHLIST":
      return state.map(book =>
        book.id === action.id ? { ...book, category: "WISHLIST" } : book
      );
    case "CURRENT":
      return state.map(book =>
        book.id === action.id ? { ...book, category: "CURRENT" } : book
      );
    case "COMPLETED":
      return state.map(book =>
        book.id === action.id ? { ...book, category: "COMPLETED" } : book
      );
    case "ADD":
      return state.map(book =>
        book.id === action.id ? { ...book, category: "WISHLIST" } : book
      );
    case "REMOVE":
      return state.map(book =>
        book.id === action.id ? { ...book, category: null } : book
      );
    default:
      return state;
  }
};

export default books;
