import React, { Component } from "react";
import { Button } from "antd";
import PropTypes from "prop-types";

class ActionToggle extends Component {
  getAction = () => {
    const { has_category } = this.props;
    return has_category ? "remove" : "add";
  };
  render() {
    const { handleAction } = this.props;
    const action = this.getAction();
    return (
      <Button
        onClick={() => {
          handleAction(action);
        }}
      >
        {action.toUpperCase()}
      </Button>
    );
  }
}

ActionToggle.propTypes = {
  has_category: PropTypes.bool.isRequired,
  handleAction: PropTypes.func.isRequired
};

export default ActionToggle;
