import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Layout, Menu, Divider, Row } from "antd";
import Book from "./book";
import { CATEGORIES, switchMenu, ACTIONTYPE } from "../actions";

class BookShelve extends Component {
  showBooks = books => {
    const count = books.length;
    if (!count) {
      return <span>No books added yet</span>;
    }
    return (
      <React.Fragment>
        <h3>
          Showing {count} book{count > 1 && "s"}
        </h3>
        <div>
          {books.map((book, index) => {
            const params = {
              ...book,
              showTag: false,
              actionType: ACTIONTYPE.DROPDOWN
            };
            return <Book {...params} key={index} />;
          })}
        </div>
      </React.Fragment>
    );
  };
  render() {
    const { index, category, switchMenu } = this.props,
      books = this.props[`${category}`];

    return (
      <React.Fragment>
        <Divider>Categories</Divider>
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Layout.Sider width={200} style={{ background: "#fff" }}>
            <Menu
              mode="inline"
              defaultSelectedKeys={[index.toString()]}
              style={{ height: "100%" }}
              onSelect={switchMenu}
            >
              {Object.values(CATEGORIES).map((item, index) => {
                return (
                  <Menu.Item title={item.val} key={index}>
                    {item.desc}
                  </Menu.Item>
                );
              })}
            </Menu>
          </Layout.Sider>
          <Layout.Content style={{ padding: "0 24px" }}>
            <Row gutter={8}>{this.showBooks(books)}</Row>
          </Layout.Content>
        </Layout>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  index: state.book_navigation.index,
  category: state.book_navigation.category,
  CURRENT: state.books.filter(book => book.category === CATEGORIES.CURRENT.val),
  WISHLIST: state.books.filter(
    book => book.category === CATEGORIES.WISHLIST.val
  ),
  COMPLETED: state.books.filter(
    book => book.category === CATEGORIES.COMPLETED.val
  )
});

const mapDispatchToProps = dispatch => ({
  switchMenu: ({ key, item }) =>
    dispatch(switchMenu({ index: parseInt(key), category: item.props.title }))
});

BookShelve.propTypes = {
  index: PropTypes.number.isRequired,
  category: PropTypes.string.isRequired,
  CURRENT: PropTypes.array,
  WISHLIST: PropTypes.array,
  COMPLETED: PropTypes.array,
  switchMenu: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookShelve);
