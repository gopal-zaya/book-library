import React, { Component } from "react";
import { Select } from "antd";
import PropTypes from "prop-types";
import { CATEGORIES } from "../actions";

class ActionDropdown extends Component {
  render() {
    const { book_category, handleAction } = this.props;
    return (
      <Select
        value={book_category || "Add to"}
        dropdownMatchSelectWidth={false}
        onSelect={handleAction}
      >
        {Object.values(CATEGORIES).map((category, i) => {
          return (
            <Select.Option value={category.val} key={i}>
              {category.desc}
            </Select.Option>
          );
        })}
      </Select>
    );
  }
}

ActionDropdown.propTypes = {
  book_category: PropTypes.string,
  handleAction: PropTypes.func.isRequired
};

export default ActionDropdown;
