import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Layout, Divider, Input, Row, Col } from "antd";
import { searchQuery, ACTIONTYPE } from "../actions";
import Book from "./book";

class BookSearch extends Component {
  render() {
    const { books, search, search_query } = this.props,
      book_count = books.length;
    return (
      <React.Fragment>
        <Layout.Content style={{ padding: "0 24px" }}>
          <Row gutter={8}>
            <Col>
              <Input.Search
                placeholder="Search book by name"
                defaultValue={search_query}
                onSearch={search}
                style={{ marginTop: "20px", width: "40%" }}
                enterButton
              />
              <Divider>
                {book_count} book{book_count > 1 && "s"} listed
              </Divider>
            </Col>
            {books.map((book, index) => {
              const params = {
                ...book,
                showTag: true,
                actionType: ACTIONTYPE.TOGGLE
              };
              return <Book {...params} key={index} />;
            })}
          </Row>
        </Layout.Content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  books: state.books.filter(book =>
    book.name
      .toLocaleLowerCase()
      .includes(state.search_query.toLocaleLowerCase())
  ),
  search_query: state.search_query
});
const mapDispatchToProps = dispatch => ({
  search: query => dispatch(searchQuery(query))
});

BookSearch.propTypes = {
  books: PropTypes.array,
  search_query: PropTypes.string,
  search: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookSearch);
