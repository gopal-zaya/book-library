import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Card, Tag } from "antd";
import ActionDropdown from "./actionDropdown";
import ActionToggle from "./actionToggle";
import {
  addToCompletion,
  addToWishList,
  addToCurrentlyReading,
  add,
  remove,
  CATEGORIES,
  ACTIONTYPE
} from "../actions";

class Book extends Component {
  handleAction = val => {
    const { id } = this.props;
    this.props[val.toLocaleLowerCase()](id);
  };
  showDescription = () => {
    const { author, showTag, category } = this.props;
    if (showTag && category) {
      return (
        <React.Fragment>
          <p>By {author}</p>
          {
            <Tag color={CATEGORIES[category].color}>
              {CATEGORIES[category].desc}
            </Tag>
          }
        </React.Fragment>
      );
    }
    return `By ${author}`;
  };
  showAction = () => {
    const { actionType, category } = this.props;
    if (actionType === ACTIONTYPE.TOGGLE) {
      return [
        <ActionToggle
          has_category={!!category}
          handleAction={this.handleAction}
        />
      ];
    } else if (actionType === ACTIONTYPE.DROPDOWN) {
      return [
        <ActionDropdown
          book_category={category}
          handleAction={this.handleAction}
        />
      ];
    }
  };
  render() {
    const { name, image_uri } = this.props;
    return (
      <Card
        hoverable
        style={{
          width: "22%",
          margin: "20px 20px 0 0",
          display: "inline-block"
        }}
        cover={<img alt={name} src={image_uri} />}
        actions={this.showAction()}
      >
        <Card.Meta title={name} description={this.showDescription()} />
      </Card>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  current: id => dispatch(addToCurrentlyReading(id)),
  wishlist: id => dispatch(addToWishList(id)),
  completed: id => dispatch(addToCompletion(id)),
  add: id => dispatch(add(id)),
  remove: id => dispatch(remove(id))
});

Book.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  author: PropTypes.string,
  category: PropTypes.string,
  image_uri: PropTypes.string,
  showTag: PropTypes.bool,
  actionType: PropTypes.string,
  CURRENT: PropTypes.func,
  WISHLIST: PropTypes.func,
  COMPLETED: PropTypes.func,
  add: PropTypes.func,
  remove: PropTypes.func
};

export default connect(
  null,
  mapDispatchToProps
)(Book);
