import React, { Component } from "react";
import BookShelve from "./book-shelve";
import BookSearch from "./book-search";
import {
  BrowserRouter as Router,
  Route,
  Link,
  withRouter
} from "react-router-dom";
import { Layout, Menu } from "antd";
import "antd/dist/antd.css";

const NavBar = withRouter(props => {
  const { location } = props;
  return (
    <Menu
      theme="dark"
      mode="horizontal"
      style={{ lineHeight: "64px" }}
      selectedKeys={[location.pathname]}
    >
      <Menu.Item key="/">
        <Link to="/">Book Shelve</Link>
      </Menu.Item>
      <Menu.Item key="/search">
        <Link to="/search">Search</Link>
      </Menu.Item>
    </Menu>
  );
});
class Home extends Component {
  render() {
    return (
      <Router>
        <Layout>
          <Layout.Header className="header">
            <NavBar />
          </Layout.Header>
          <Layout.Content style={{ padding: "0 50px" }}>
            <Route path="/" exact component={BookShelve} />
            <Route path="/search" exact component={BookSearch} />
          </Layout.Content>
          <Layout.Footer style={{ textAlign: "center" }}>
            Vedantu ©2018 Created by Gopal Ojha
          </Layout.Footer>
        </Layout>
      </Router>
    );
  }
}

export default Home;
