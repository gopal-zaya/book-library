export const addToCompletion = id => ({
  type: "COMPLETED",
  id
});
export const addToWishList = id => ({
  type: "WISHLIST",
  id
});
export const addToCurrentlyReading = id => ({
  type: "CURRENT",
  id
});
export const add = id => ({
  type: "ADD",
  id
});
export const remove = id => ({
  type: "REMOVE",
  id
});
export const searchQuery = query => ({
  type: "SEARCH",
  query
});
export const switchMenu = ({ index, category }) => ({
  type: "SWITCH_MENU",
  index,
  category
});

export const CATEGORIES = {
  CURRENT: { val: "CURRENT", desc: "Currently reading", color: "green" },
  WISHLIST: { val: "WISHLIST", desc: "Want to read", color: "blue" },
  COMPLETED: { val: "COMPLETED", desc: "Done reading", color: "orange" }
};
export const ACTIONTYPE = {
  DROPDOWN: "DROPDOWN",
  TOGGLE: "TOGGLE"
};
